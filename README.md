## Wikidata Downloader

The latest wikidata download can be found [here](https://dumps.wikimedia.org/wikidatawiki/entities/).

Wikimedia publishes a new dump once every few days. The releases are not on
fixed dates. We use the latest-truthy.nt.bz2 file as basis for the import.

Each time an update is posted the http `Last-Modified` header changes. The downloader will only
download the file, when it changed from the last deployment!

The date is stored in zookeeper under /wikidata/downloader/last_modified.

There it will be read from another service.