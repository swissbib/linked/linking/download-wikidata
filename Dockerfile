FROM python:3.8-slim-buster
COPY requirements.txt /tmp
RUN pip install -r /tmp/requirements.txt
COPY src/script.py /src/script.py
USER swissbib

ENTRYPOINT [ "python" ]
CMD [ "/src/script.py"]